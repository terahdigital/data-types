<?php declare(strict_types=1);

namespace Terah\Types;


use Closure;
use ArrayObject;
use JsonSerializable;
use stdClass;
use Terah\Assert\Assert;

class ArrObj extends ArrayObject implements JsonSerializable
{
    protected $dirty                = [];

    protected $trackChanges         = false;


    public function __construct($input=[], int $flags=3, string $iterator_class="ArrayIterator")
    {
        $input                  = $input instanceof ArrayObject ? $input->getArrayCopy() : $input;
        parent::__construct($input, $flags, $iterator_class);
    }


    public function has($value) : bool
    {
        return in_array($value, $this->getArrayCopy());
    }


    public function trackChanges(bool $track=true) : ArrObj
    {
        $this->trackChanges     = $track;

        return $this;
    }


    public function isUnchanged() : bool
    {
        if ( ! empty($this->dirty) )
        {
            return false;
        }

        return true;
    }


    public function getChanged() : array
    {
        return array_keys($this->dirty);
    }


    public function getChangedOutput() : string
    {
        $out                    = [];
        foreach ( $this->dirty as $key => $values )
        {
            list($old, $new)        = $values;
            $out[]                  = "{$key}: FROM ({$old}) TO ({$new})";
        }

        return implode("\n", $out);
    }

    /**
     * @param string|array $names
     * @return bool
     */
    public function hasChanged($names) : bool
    {
        $names                  = is_array($names) ? $names : [$names];
        Assert::that($names)->all()->isString()->notEmpty();

        return ! empty(array_intersect(array_keys($this->dirty), $names));
    }


    /**
     * @param array|ArrayObject|stdClass $data
     */
    public function merge($data)
    {
        if ( is_object($data) )
        {
            $data                   = $data instanceof ArrayObject ? $data->getArrayCopy() : (array)$data;
        }
        Assert::that($data)->isArray();
        $this->exchangeArray(array_merge($this->getArrayCopy(), $data));
    }


    public function filter(Closure $callback) : ArrObj
    {
        foreach ( $this->getArrayCopy() as $idx => $value )
        {
            if ( ! $callback->__invoke($value, $idx) )
            {
                $this->offsetUnset($idx);
            }
        }

        return $this;
    }


    public function keys() : Arr
    {
        return new Arr(array_keys($this->getArrayCopy()));
    }


    public function keyExist($key) : bool
    {
        return $this->offsetExists($key);
    }


    public function values() : Arr
    {
        return new Arr(array_values($this->getArrayCopy()));
    }


    public function jsonSerialize()
    {
        return $this->getArrayCopy();
    }


    public function offsetSet($key, $value)
    {
        if ( $this->trackChanges )
        {
            if ( ! $this->offsetExists($key) || $this->offsetGet($key) !== $value )
            {
                $this->dirty[$key]      = [$this->offsetGet($key), $value];
            }
        }
        parent::offsetSet($key, $value);
    }


    public function offsetUnset($key)
    {
        if ( $this->offsetExists($key) )
        {
            parent::offsetUnset($key);
        }
    }


    public function toArray(bool $deep=true) : array
    {
        $data                   = $this->getArrayCopy();
        foreach ( $data as $idx => $datum )
        {
            if ( $datum instanceof ArrObj )
            {
                $data[$idx]         = $datum->toArray($deep);
            }
        }

        return $data;
    }


    public function setObject(ArrObj $object) : ArrObj
    {
        foreach ( $object as $key => $value )
        {
            $this->offsetSet($key, $value);
        }

        return $this;
    }
}