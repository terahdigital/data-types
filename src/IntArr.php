<?php declare(strict_types=1);

namespace Terah\Types;


use Terah\Assert\Assert;

class IntArr extends Arr
{
    /**
     * @param int $key
     * @param int $value
     */
    public function offsetSet($key, $value)
    {
        Assert::that($key)->int();
        Assert::that($value)->int();

        parent::offsetSet($key, $value);
    }

    /**
     * @param int $key
     * @return int
     */
    public function offsetGet($key)
    {
        Assert::that($key)->int();

        return parent::offsetGet($key);
    }
}