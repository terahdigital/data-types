<?php declare(strict_types=1);

namespace Terah\Types;

use ArrayObject;
use JsonSerializable;

class ArrCol extends ArrayObject implements JsonSerializable
{
    public function __construct(array $values=[])
    {
        $values                 = ! is_null($values) ? $values : [];
        foreach ( $values as $idx => $value )
        {
            $this->offsetSet($idx, $value);
        }
    }


    public function keyExist($key) : bool
    {
        return $this->offsetExists($key);
    }


    public function offsetSet($key, $value)
    {
        $value                  = $value instanceof ArrObj ? $value : new ArrObj($value);
        parent::offsetSet($key, $value);
    }

    public function append($value)
    {
        $value                  = $value instanceof ArrObj ? $value : new ArrObj($value);

        parent::append($value);
    }


    public function jsonSerialize()
    {
        return $this->getArrayCopy();
    }


    public function values() : Arr
    {
        return new Arr(array_values($this->getArrayCopy()));
    }
}