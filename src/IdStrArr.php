<?php declare(strict_types=1);

namespace Terah\Types;


use Terah\Assert\Assert;

class IdStrArr extends Arr
{
    /**
     * @param int $key
     * @param string $value
     */
    public function offsetSet($key, $value)
    {
        Assert::that($key)->int();
        Assert::that($value)->string();

        parent::offsetSet($key, $value);
    }

    /**
     * @param int $key
     * @return string
     */
    public function offsetGet($key)
    {
        Assert::that($key)->int();

        return parent::offsetGet($key);
    }
}