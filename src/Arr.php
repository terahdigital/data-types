<?php declare(strict_types=1);

namespace Terah\Types;


use ArrayObject;
use Terah\Assert\Assert;

class Arr extends ArrObj
{
    // Keys can only be numeric
    public function __construct($input=[], int $flags=3, string $iterator_class="ArrayIterator")
    {
        parent::__construct([], $flags, $iterator_class);
        $input                  = $input instanceof ArrayObject ? $input->getArrayCopy() : $input;
        Assert::that($input)->isTraversable();

        foreach ( $input as $key => $value )
        {
            $this->offsetSet($key, $value);
        }
    }

    /**
     * @return IntArr
     */
    public function keys() : Arr
    {
        return parent::keys();
    }

    /**
     * @param int $key
     * @param mixed $value
     */
    public function offsetSet($key, $value)
    {
        Assert::that($key)->int();

        parent::offsetSet($key, $value);
    }

    /**
     * @param int $key
     * @return mixed
     */
    public function offsetGet($key)
    {
        Assert::that($key)->int();

        return parent::offsetGet($key);
    }
}