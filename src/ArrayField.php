<?php declare(strict_types=1);


namespace Terah\Types;


use Closure;

class ArrayField extends Field
{
    public function __construct(string $name, Closure $validator, $default=null)
    {
        parent::__construct($name, 'array', $validator, $default);
    }
}