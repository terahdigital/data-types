<?php declare(strict_types=1);

namespace Terah\Types;

use Terah\Assert\Assert;
use ArrayObject;
use JsonSerializable;
use Closure;
use stdClass;


class DTO extends ArrayObject implements JsonSerializable
{
    protected array $defaults         = [];

    /** @var Field[]|Closure[] */
    protected array $schema           = [];


    public function __construct($input=[], bool $skipExtraFields=false)
    {
        $input                  = $input instanceof ArrayObject ? $input->getArrayCopy() : (array)$input;
        $input                  = array_merge($this->defaults, $input);
        $this->populateSchema();
        parent::__construct([], ArrayObject::STD_PROP_LIST|ArrayObject::ARRAY_AS_PROPS);
        $input                  = $input ?: [];
        $this->setValues($input, $skipExtraFields);
    }


    public function setValues(array $input, bool $skipExtraFields=true) : DTO
    {
        foreach ( $input as $field => $value )
        {
            if ( $skipExtraFields && ! array_key_exists($field, $this->schema) )
            {
                continue;
            }
            $this->offsetSet($field, $value);
        }

        return $this;
    }

    /**
     * @param stdClass|array|string     $values
     * @param null $value
     * @return $this
     */
    public function set($values, $value=null)
    {
        if ( is_object($values) )
        {
            $values                 = $values instanceof ArrayObject ? $values->getArrayCopy() : (array)$values;
        }
        if ( ! is_array($values) )
        {
            $values                 = [$values => $value];
        }
        foreach ( $values as $field => $value )
        {
            $this->offsetSet($field, $value);
        }

        return $this;
    }

    /**
     * @param mixed $key
     * @param mixed $value
     */
    public function offsetSet($key, $value)
    {
        $value                  = $this->validate($key, $value);
        parent::offsetSet($key, $value);
    }

    /**
     * @param mixed $key
     * @return mixed
     */
    public function offsetGet($key)
    {
        $this->assert($key)->string('The property name must be a string')->notEmpty('The property name must not be empty');
        $this->assert($this->schema)->keyExists($key, get_called_class() . ' does not contain the property "%s"');

        return parent::offsetGet($key);
    }

    /**
     * @param Field $field
     * @return DTO
     */
    public function addField(Field $field) : DTO
    {
        $name                   = $field->getName();
        $this->assert($name)->notEmpty();
        $this->schema[$name]    = $field;

        return $this;
    }

    /**
     * @param $name
     * @param $value
     * @return bool|mixed
     */
    public function validate(string $name, $value)
    {
        $this->assert($name)->string('The property name must be a string')->notEmpty('The property name must not be empty');
        $this->assert($this->schema)->keyExists($name, get_called_class() . ' does not contain the property "%s"');
        if ( $this->schema[$name] instanceof Field )
        {
            $type                   = $this->schema[$name]->getType();
            $record                 = (object)$this->getArrayCopy();

            return $this->schema[$name]->getValidator()->__invoke($name, $value, $type, $record);
        }
        if ( $this->schema[$name] instanceof Closure )
        {
            $this->schema[$name]->__invoke($name, $value);
        }

        return $value;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $data   = $this->getArrayCopy();
        foreach ( $data as $field => $value )
        {
            $data[$field]           = $value instanceof DTO ? $this->getArrayCopy() : $value;
        }

        return $data;
    }

    /**
     * @return $this
     */
    protected function populateSchema() : DTO
    {
        $this->schema           = [];

        return $this;
    }

    /**
     * @param bool $getForeign
     * @return array
     */
    public function getSchema(bool $getForeign=false) : array
    {
        if ( $getForeign )
        {
            return $this->schema;
        }
        return array_filter($this->schema, function(Field $field) {

            return ! in_array($field->getType(), ['collection', 'foreign']);
        });
    }

    /**
     * @param array $data
     * @param bool  $skipExtraFields
     * @return static
     */
    static public function factory($data=[], bool $skipExtraFields=false)
    {
        return new static($data, $skipExtraFields);
    }

    /**
     * @param $value
     * @return Assert
     */
    protected function assert($value) : Assert
    {
        return new Assert($value);
    }

    /**
     * @return string
     */
    public function serialize()
    {
        return serialize($this->getArrayCopy());
    }

    /**
     * @param string $data
     */
    public function unserialize($data)
    {
        $this->setFlags(ArrayObject::STD_PROP_LIST|ArrayObject::ARRAY_AS_PROPS);
        $this->populateSchema();
        $this->exchangeArray(unserialize($data));
    }

    public function toArrObj() : ArrObj
    {
        return new ArrObj($this->getArrayCopy());
    }

    /**
     * @param string $field
     * @param mixed $value
     * @param string $fieldType
     * @return float|int|null|string
     */
    protected function fixType(string $field, $value, string $fieldType)
    {
        Assert::that($value)->nullOr()->scalar("var is type of " . gettype($value));

        if ( is_null($value) )
        {
            return null;
        }
        // return on null, '' but not 0
        if ( ! is_numeric($value) && empty($value) )
        {
            return null;
        }
        // Don't cast invalid values... only those that can be cast cleanly
        switch ( $fieldType )
        {
            case 'varchar':
            case 'text';
            case 'date':
            case 'datetime':
            case 'timestamp':
            case 'foreign':

                // return on null, '' but not 0
                return ! is_numeric($value) && empty($value) ? null : (string)$value;

            case 'int':

                if ( $field === 'id' || substr($field, -3) === '_id' )
                {
                    return $value ? (int)$value : null;
                }

                return ! is_numeric($value) ? null : (int)$value;

            case 'decimal':

                return ! is_numeric($value) ? null : (float)$value;

            case 'bool':
            case 'boolean':

                return is_bool($value) ? $value : (bool)($value);

            default:

                return $value;
        }
    }


    public function whatsChanged(DTO $old) : ArrObj
    {
        $changes                = array_diff_assoc($this->getArrayCopy(), $old->getArrayCopy());
        $result                 = new ArrObj();
        foreach ( $changes as $key => $change )
        {
            if ( in_array($key, ['creator_id', 'creator', 'created_ts', 'modifier_id', 'modifier', 'modified_ts', 'deleter_id', 'deleter', 'deleted_ts', 'active']) || static::endsWith("_id", $key) )
            {
                continue;
            }
            if ( isset($old->{$key}) )
            {
                $result[$key]               = [$old->{$key}, $change];
            }
        }

        return $result;
    }

    /**
     * @param string $needle
     * @param string $hayStack
     * @return bool
     */
    public static function endsWith(string $needle, string $hayStack) : bool
    {
        $needle         = strrev($needle);
        $hayStack       = strrev($hayStack);

        return strpos($hayStack, $needle) === 0;
    }

}
