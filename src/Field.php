<?php declare(strict_types=1);


namespace Terah\Types;

use Closure;
use Terah\Assert\Assert;


class Field
{
    /** @var string */
    protected $name             = '';

    /** @var mixed */
    protected $default          = null;

    /** @var string */
    protected $type             = 'varchar';

    /** @var Closure */
    protected $validator        = null;

    /** @var mixed */
    protected $value            = null;


    public function __construct(string $name, string $type, Closure $validator, $default)
    {
        $this->name             = $name;
        $this->validator        = $validator;
        $this->type             = $type;
        $this->default          = $default;
    }


    public function validate()
    {
        $this->validator->__invoke($this);
    }

    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Field
     */
    public function setName(string $name) : Field
    {
        Assert::that($name)->notEmpty();
        $this->name             = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDefault()
    {
        return $this->default;
    }

    /**
     * @param mixed $default
     * @return Field
     */
    public function setDefault($default)
    {
        $this->default          = $default;

        return $this;
    }

    /**
     * @return string
     */
    public function getType() : string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return Field
     */
    public function setType(string $type) : Field
    {
        Assert::that($type)->notEmpty();
        $this->type             = $type;

        return $this;
    }

    /**
     * @return Closure
     */
    public function getValidator() : Closure
    {
        return $this->validator;
    }

    /**
     * @param Closure $validator
     * @return Field
     */
    public function setValidator(Closure $validator) : Field
    {
        $this->validator        = $validator;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     * @return Field
     */
    public function setValue($value)
    {
        $this->value            = $value;
        $this->fixType();

        return $this;
    }


    public function fixType()
    {
        Assert::that($this->value)->nullOr()->scalar("var is type of " . gettype($this->value));

        if ( is_null($this->value) )
        {
            return ;
        }
        // return on null, '' but not 0
        if ( ! is_numeric($this->value) && empty($this->value) )
        {
            $this->value            = null;

            return ;
        }
        // Don't cast invalid values... only those that can be cast cleanly
        switch ( $this->type )
        {
            case 'varchar':
            case 'text';
            case 'date':
            case 'datetime':
            case 'timestamp':
            case 'foreign':

                // return on null, '' but not 0
                $this->value            =  ! is_numeric($this->value) && empty($this->value) ? null : (string)$this->value;

                return ;

            case 'int':

                if ( $this->name === 'id' || substr($this->name, -3) === '_id' )
                {
                    $this->value            = $this->value ? (int)$this->value : null;

                    return ;
                }

                $this->value            =  ! is_numeric($this->value) ? null : (int)$this->value;

                return ;

            case 'decimal':

                $this->value            =  ! is_numeric($this->value) ? null : (float)$this->value;

                return ;

            case 'bool':
            case 'boolean':

                $this->value                =  is_bool($this->value) ? $this->value : (bool)($this->value);
        }
    }

}