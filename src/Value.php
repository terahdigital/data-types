<?php declare(strict_types=1);


namespace Terah\Types;

use Terah\Assert\Assert;
use Terah\Assert\AssertionFailedException;
use JsonSerializable;
use ArrayObject;

/**
 * Class FieldCollection
 *
 * @package Terah\Types
 */
abstract class Value extends ArrayObject implements JsonSerializable
{
    /** @var Field[]  */
    protected array $schema   = [];

    protected $errors   = [];

    public function __construct(array $input=[])
    {
        $input              = $input ?: [];
        $this->populateSchema();
        parent::__construct([], ArrayObject::STD_PROP_LIST| ArrayObject::ARRAY_AS_PROPS);
        $this->setByArray($input);
    }

    abstract protected function populateSchema() : Value;


    public function setByArray(array $data) : Value
    {
        $this->clearErrors();
        foreach ( $this->schema as $fieldName => $fieldObj )
        {
            $this->offsetSet($fieldName, $fieldObj->getDefault());
            if ( ! isset($data[$fieldName]) )
            {
                continue;
            }
            $value              = is_string($data[$fieldName]) ? trim(strip_tags($data[$fieldName])) : $data[$fieldName];
            $fieldObj           = $this->schema[$fieldName];
            switch ( $fieldObj->getType() )
            {
                case 'int':

                    $this->offsetSet($fieldName, is_numeric($value) ? (int)$value : $value);

                    continue;

                case 'float':

                    $this->offsetSet($fieldName, is_numeric($value) ? (float)$value : $value);

                    continue;

                case 'bool':

                    $this->offsetSet($fieldName, is_numeric($value) || is_bool($value) ? (bool)$value : $value);

                    continue;

                default:

                    $this->offsetSet($fieldName, $value);

                    continue;
            }
        }

        return $this;
    }


    public function hasErrors() : bool
    {
        return ! empty($this->errors);
    }


    public function clearErrors() : Value
    {
        $this->errors = [];

        return $this;
    }

    /**
     * @param mixed $key
     * @param mixed $value
     */
    public function offsetSet($key, $value)
    {
        Assert::that($key)->string('The property name must be a ')->notEmpty('The property name must not be empty');
        Assert::that($this->schema)->keyExists($key, get_called_class() . ' does not contain the property "%s"');
        parent::offsetSet($key, $value);
    }

    /**
     * @param mixed $key
     * @return mixed
     */
    public function offsetGet($key)
    {
        Assert::that($key)->string('The property name must be a string')->notEmpty('The property name must not be empty');
        Assert::that($this->schema)->keyExists($key, get_called_class() . ' does not contain the property "%s"');

        return parent::offsetGet($key);
    }


    public function validate() : bool
    {
        $this->errors = [];
        foreach ( $this->getArrayCopy() as $field => $value )
        {
            $callback = $this->schema[$field]->getValidator() ?? function($value) {};
            try
            {
                $callback->__invoke($value);
            }
            catch ( AssertionFailedException $e )
            {
                $this->setError($field, $e->getMessage());
            }
        }

        return empty($this->errors);
    }


    public function setError(string $field, string $error) : Value
    {
        $this->errors[$field][] = $error;

        return $this;
    }


    public function getErrors() : array
    {
        return $this->errors;
    }


    public function getErrorsAsString() : string
    {
        $errors = [];
        foreach ( $this->errors as $field => $message )
        {
            $errorMessage = implode("\n",$message);

            array_push($errors, "{$field}: {$errorMessage}");
        }

        return implode("\n", $errors);
    }


    public function jsonSerialize()
    {
        return $this->getArrayCopy();
    }


    public function getSetValues() : array
    {
        $setValues = $this->getArrayCopy();
        foreach ( $setValues as $field => $value )
        {
            if ( is_null($value) )
            {
                unset($setValues[$field]);
            }
        }

        return $setValues;
    }


    public function getSetValuesToJson() : string
    {
        return json_encode($this->getSetValues());
    }
}