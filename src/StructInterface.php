<?php declare(strict_types=1);

namespace Terah\Types;

use Closure;
use stdClass;

interface StructInterface
{
    public function setMutable(stdClass $data, bool $permissive=false) : StructInterface;

    /**
     * @param stdClass   $data
     * @param bool|false $permissive
     * @return StructInterface
     */
    public function setWritable(stdClass $data, bool $permissive=false) : StructInterface;

    /**
     * @param stdClass   $data
     * @param string|array $types
     * @param bool|false $permissive
     * @return StructInterface
     */
    public function setFiltered(stdClass $data, $types, bool $permissive=false) : StructInterface;

    /**
     * @param string     $group
     * @param stdClass   $data
     * @param bool|false $permissive
     * @return StructInterface
     */
    public function setByGroup($group, stdClass $data, bool $permissive=false) : StructInterface;

    /**
     * @param stdClass      $data
     * @param string|array  $types
     * @param bool          $permissive
     * @return stdClass
     */
    public function filterData(stdClass $data, $types=null, bool $permissive=false);

                    /** @var Struct|StructCollection $struct */
    /**
     * @param stdClass $data
     * @return StructInterface
     */
    public function spawn(stdClass $data) : StructInterface;

    /**
     * @param bool $hard
     * @return StructInterface
     */
    public function reset(bool $hard=false) : StructInterface;

    /**
     * @return bool
     */
    public function isUnchanged() : bool;

    /**
     * @return array
     */
    public function whatsChanged() : array;

    /**
     * @param string|array $names
     * @return bool
     */
    public function hasChanged($names) : bool;

    /**
     * @param string|stdClass $name
     * @param mixed $value
     * @param bool $permissive
     * @param bool|null $clearConflicts
     *
     * @return StructInterface
     */
    public function set($name, $value=null, bool $permissive=false, $clearConflicts=null) : StructInterface;

    /**
     * @param stdClass|array   $object
     * @param bool|false $permissive
     * @param bool|null $clearConflicts
     * @return StructInterface
     */
    public function setObject($object, bool $permissive=false, $clearConflicts=null) : StructInterface;

    /**
     * @param string $name
     * @param null   $value
     * @param bool|false $permissive
     * @return mixed
     */
    public function formatField(string $name, $value=null, bool $permissive=false);

    /**
     * @return StructInterface
     */
    public function checkRequired() : StructInterface;

    /**
     * @param stdClass|string $name
     * @param mixed|Closure       $value
     * @param bool $permissive
     * @return StructInterface
     */
    public function setIfEmpty($name, $value=null, bool $permissive=false) : StructInterface;

    /**
     * @param int $id
     * @param bool|false $permissive
     * @return StructInterface
     */
    public function setId(int $id, bool $permissive=false) : StructInterface;

    /**
     * @return int
     */
    public function getId() : int;


    /**
     * @param bool $includeTypes
     * @return array
     */
    public function getFields(bool $includeTypes=false) : array;

    /**
     * @param string $metaGroup
     * @return array
     */
    public function getFieldsByMetaGroup(string $metaGroup) : array;

    /**
     *
     * @param $metaGroup
     * @param bool $removeNulls
     * @return array
     */
    public function getValuesByMetaGroup(string $metaGroup, bool $removeNulls=false) : array;


    /**
     * @param bool $includeTypes
     * @return array
     */
    public function getAllFields(bool $includeTypes=false) : array;

    /**
     * @param bool $includeTypes
     * @return array
     */
    public function getStructFields(bool $includeTypes=false) : array;

    /**
     * @return array
     */
    public function getAliasedFields() : array;

    /**
     * @param string $field
     * @param bool|true $includeCollections
     * @return bool|string
     */
    public function getStructFieldType(string $field, bool $includeCollections=true) : string;



    public function isField(string $name, string $metaGroup=null) : bool;


    public function getFieldType(string $name) : string;

    /**
     * @param string[] $names
     */
    public function assertFieldsExists(array $names);

    /**
     * @param string $name
     * @param mixed $value
     * @return StructInterface
     */
    public function __set(string $name, $value) : StructInterface;

    /**
     * @param $name
     */
    public function __unset(string $name);

    /**
     * @param string|null $name
     * @return mixed|stdClass
     */
    public function get(string $name=null);

    /**
     * @param string|null $field
     * @param bool $returnDiff
     * @return array|mixed|object
     */
    public function getChanged(string $field=null, bool $returnDiff=false);

    /**
     * @param array $fields
     * @return stdClass
     */
    public function getChangedFields(array $fields) : stdClass;

    /**
     * @return array
     */
    public function getArray() : array;

    /**
     * @return array
     */
    public function toArray() : array;

    /**
     * @param string $name
     * @return mixed
     */
    public function __get(string $name);

    /**
     * @param $name
     * @return bool
     */
    public function __isset(string $name) : bool;

    /**
     * @param $name
     * @return bool
     */
    public function isPopulated(string $name) : bool;

    /**
     * @return string
     */
    public function jsonSerialize();

    /**
     * @return array
     */
    public function strip() : array;

    /**
     * @param array $keys
     * @return array
     */
    public function getLoggingValues(array $keys=[]) : array;
}