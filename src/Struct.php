<?php declare(strict_types=1);

namespace Terah\Types;

use ArrayObject;
use Closure;
use JsonSerializable;
use Terah\Assert\Assert;

use stdClass;

class Struct  implements JsonSerializable, StructInterface
{
    const RESTRICTED                = [self::IMMUTABLE, self::READONLY];

    const IMMUTABLE                 = 'immutable';

    const READONLY                  = 'readonly';

    const REQUIRED                  = 'required';

    const LOGABLE                   = 'logable';

    protected array $data                 = [];

    protected array $schema               = [];

    /** @var string[] $structFields */
    protected array $structFields         = [];

    protected array $dirty                = [];

    protected array $metaGroups           = [];

    /** @var string[] */
    protected array $stripOnSerialize     = [];

    protected bool $clearConflicts       = false;

    /**
     * @param stdClass|StructInterface|array|null $data
     * @param bool $permissive
     * @param bool $clearConflicts
     */
    public function __construct($data=null, bool $permissive=false, bool $clearConflicts=false)
    {
        $this->_populateFieldProperties();
        if ( $data )
        {
            $this->setObject($data, $permissive);
        }
        $this->clearConflicts = $clearConflicts;
    }

    /**
     * @param stdClass   $data
     * @param bool|false $permissive
     * @return StructInterface
     */
    public function setMutable(stdClass $data, bool $permissive=false) : StructInterface
    {
        return $this->setFiltered($data, [self::IMMUTABLE, self::READONLY], $permissive);
    }

    /**
     * @param stdClass   $data
     * @param bool|false $permissive
     * @return StructInterface
     */
    public function setWritable(stdClass $data, bool $permissive=false) : StructInterface
    {
        return $this->setFiltered($data, [self::READONLY], $permissive);
    }

    /**
     * @param stdClass   $data
     * @param string|array $types
     * @param bool|false $permissive
     * @return StructInterface
     */
    public function setFiltered(stdClass $data, $types, bool $permissive=false) : StructInterface
    {
        $data                   = $this->filterData($data, $types, $permissive);

        return $this->setObject($data, $permissive);
    }

    /**
     * @param string     $group
     * @param stdClass   $data
     * @param bool|false $permissive
     * @return StructInterface
     */
    public function setByGroup($group, stdClass $data, bool $permissive=false) : StructInterface
    {
        $nameFields             = $this->getFieldsByMetaGroup($group);
        foreach ( $data as $field => $value )
        {
            if ( ! in_array($field, $nameFields) )
            {
                unset($data->{$field});
            }
        }

        return $this->setObject($data, $permissive);
    }

    /**
     * @param stdClass      $data
     * @param string|array  $types
     * @param bool          $permissive
     * @return stdClass
     */
    public function filterData(stdClass $data, $types=null, bool $permissive=false)
    {
        $types                  = is_array($types) ? $types : [$types];

        $readOnlyFields         = $this->getFieldsByMetaGroup(static::READONLY);
        $immutableFields        = $this->getFieldsByMetaGroup(static::IMMUTABLE);
        $allRestricted          = array_merge($readOnlyFields, $immutableFields);

        foreach ( $data as $field => $value )
        {
            if ( ! $this->isField($field) )
            {
                unset($data->{$field});

                continue;
            }
            // readonly fields are filtered for both the readonly and immutable filter modes
            if ( in_array(static::RESTRICTED, $types) && in_array($field, $allRestricted) )
            {
                unset($data->{$field});

                continue;
            }
            if ( in_array(static::READONLY, $types) && in_array($field, $readOnlyFields) )
            {
                unset($data->{$field});

                continue;
            }
            if ( in_array(static::IMMUTABLE, $types) && in_array($field, $immutableFields) )
            {
                unset($data->{$field});

                continue;
            }
            if ( ( $structType = $this->getStructFieldType($field) ) )
            {
                if ( ! $value instanceof $structType )
                {
                    /** @var Struct|StructCollection $struct */
                    $struct                 = new $structType();
                    $value                  = $struct instanceof StructCollection ? (array)$value : (object)$value;
                    $value                  = $struct->filterData($value, $types, $permissive);
                }
                $data->{$field} =  $value;
            }
        }

        return $data;
    }

    /**
     * @param stdClass $data
     * @return StructInterface
     */
    public function spawn(stdClass $data) : StructInterface
    {
        $spawn                  = clone $this;

        return $spawn->reset()->set($data, null, true, false)->reset();
    }

    /**
     * @param bool $hard
     * @return StructInterface
     */
    public function reset(bool $hard=false) : StructInterface
    {
        $this->dirty            = [];
        if ( $hard )
        {
            $this->data             = [];
        }
        foreach ( $this->structFields as $field => $structType )
        {
            if ( ! empty($this->data[$field]) )
            {
                $this->data[$field]->reset($hard);
            }
        }

        return $this;
    }

    /**
     * @return bool
     */
    public function isUnchanged() : bool
    {
        if ( ! empty($this->dirty) )
        {
            return false;
        }
        foreach ( $this->structFields as $field => $structType )
        {
            if ( ! empty($this->data[$field]) && ! $this->data[$field]->isUnchanged() )
            {
                return false;
            }
        }

        return true;
    }

    /**
     * @return array
     */
    public function whatsChanged() : array
    {
        return array_keys($this->dirty);
    }

    /**
     * @param string|array $names
     * @return bool
     */
    public function hasChanged($names) : bool
    {
        $names                  = is_array($names) ? $names : [$names];
        Assert::that($names)->all()->string()->notEmpty();

        return ! empty(array_intersect(array_keys($this->dirty), $names));
    }

    /**
     * @param string|stdClass $name
     * @param mixed $value
     * @param bool $permissive
     * @param bool|null $clearConflicts
     *
     * @return StructInterface
     */
    public function set($name, $value=null, bool $permissive=false, $clearConflicts=null) : StructInterface
    {
        if ( is_object($name) )
        {
            return $this->setObject($name, $permissive, $clearConflicts);
        }
        $value                  = $this->formatField($name, $value, $permissive);
        $this->_setData($name, $value, $clearConflicts);

        return $this;
    }

    /**
     * @param stdClass|array|StructInterface|ArrayObject   $object
     * @param bool|false $permissive
     * @param bool|null $clearConflicts
     * @return StructInterface
     */
    public function setObject($object, bool $permissive=false, $clearConflicts=null) : StructInterface
    {
        if ( is_object($object) && method_exists($object, 'getArrayCopy') )
        {
            $object = $object->getArrayCopy();
        }
        $object = is_array($object) ? (object)$object : $object;
        Assert::that($object)->isObject();
        // Set the id first to force which mode we are operating in (insert or update)
        if ( property_exists($object, 'id') )
        {
            $this->setId($object->id, $permissive);
        }
        if ( $permissive )
        {
            $object = $this->filterData($object);
        }
        foreach ( $object as $key => $value )
        {
            $this->set($key, $value, $permissive, $clearConflicts);
        }

        return $this;
    }

    /**
     * @param string $name
     * @param null   $value
     * @param bool|false $permissive
     * @return mixed
     */
    public function formatField(string $name, $value=null, bool $permissive=false)
    {
        $this->assertFieldsExists([$name]);
        if ( in_array($name, $this->getFieldsByMetaGroup(static::READONLY)) )
        {
            Assert::that($permissive)->true("You cannot set the value of readonly fields ({$name})");
        }
        // Immutable fields can't be changed on update (only create)
        if ( ( $this->isField('id') && ! empty($this->data['id']) ) && in_array($name, $this->getFieldsByMetaGroup(static::IMMUTABLE)) )
        {
            Assert::that($permissive)->true("You cannot set the value of immutable fields ({$name})");
        }

        return $this->_getFieldHandler($name)->__invoke($name, $value, $permissive);
    }

    /**
     * @return StructInterface
     */
    public function checkRequired() : StructInterface
    {
        foreach ( $this->getFieldsByMetaGroup(static::REQUIRED) as $field )
        {
            Assert::that($this->data)->keyExists($field);
            Assert::that($this->data[$field])->notEmpty();
        }

        return $this;
    }

    /**
     * @param stdClass|string $name
     * @param mixed|Closure       $value
     * @param bool $permissive
     * @return StructInterface
     */
    public function setIfEmpty($name, $value=null, bool $permissive=false) : StructInterface
    {
        if ( is_object($name) )
        {
            foreach ( $name as $key => $value )
            {
                $this->setIfEmpty($key, $value, $permissive);
            }

            return $this;
        }
        $this->data = is_array($this->data) ? $this->data : [];
        if ( ! array_key_exists($name, $this->data) || is_null($this->data[$name]) )
        {
            if ( is_callable($value) )
            {
                $value = $value->__invoke($name);
            }
            $this->set($name, $value, $permissive);
        }

        return $this;
    }

    /**
     * @param int $id
     * @param bool|false $permissive
     * @return StructInterface
     */
    public function setId(int $id, bool $permissive=false) : StructInterface
    {
        return $this->set('id', $id, $permissive);
    }

    /**
     * @return int
     */
    public function getId() : int
    {
        return (int)$this->get('id');
    }

    /**
     * @param string $name
     * @param float|int|null|string $value
     * @param bool|null $clearConflicts
     */
    protected function _setData(string $name, $value, bool $clearConflicts=null)
    {
        $this->data[$name]  = ! array_key_exists($name, $this->data) ? null : $this->data[$name];
        if (  $this->data[$name] !== $value )
        {
            $this->dirty[$name] = [$this->data[$name], $value];
        }
        $this->data[$name]  = $value;
        $clearConflicts     = ! is_null($clearConflicts) ? $clearConflicts : $this->clearConflicts;
        if ( $clearConflicts )
        {
            if ( preg_match('/_id$/', $name) )
            {
                unset($this->data[preg_replace('/_id$/', '', $name)]);

                return;
            }
            unset($this->data["{$name}_id"]);
        }
    }

    /**
     * @param string $field
     * @param mixed $value
     * @param bool|false $permissive
     * @return float|int|null|string
     */
    protected function _fixType(string $field, $value, bool $permissive=false)
    {
        if ( ! $this->isField($field) && $permissive )
        {
            return $value;
        }
        $type = $this->getFieldType($field);
        if ( ( $structType = $this->getStructFieldType($field) ) )
        {
            if ( ! $value instanceof $structType )
            {
                /** @var Struct|StructCollection $struct */
                $struct     = new $structType();
                $value      = $struct instanceof StructCollection ? (array)$value : $value;
                $value      = new $structType($value, $permissive);
            }
        }
//        if ( is_null($value) )
//        {
//            return null;
//        }
//        // return on null, '' but not 0
//        if ( ! is_numeric($value) && empty($value) )
//        {
//            return null;
//        }
        // Don't cast invalid values... only those that can be cast cleanly
        switch ( $type )
        {
            case 'varchar':
            case 'text';
            case 'date':
            case 'datetime':
            case 'timestamp':

                return (string)$value;

            case 'int':

                return ! is_null($value) && ! is_numeric($value) ? $value : (int)$value;

            case 'decimal':

                return ! is_null($value) && ! is_numeric($value) ? $value : (float)$value;

            default:

                return $value;
        }
    }

    /**
     * @param bool $includeTypes
     * @return array
     */
    public function getFields(bool $includeTypes=false) : array
    {
        return $includeTypes ? $this->schema : array_keys($this->schema);
    }

    /**
     * @param string $metaGroup
     * @return array
     */
    public function getFieldsByMetaGroup(string $metaGroup) : array
    {
        $metaGroups = explode('|', $metaGroup);
        $fields     = [];
        foreach ( $metaGroups as $metaGroup )
        {
            $fields = array_merge($fields, ! empty($this->metaGroups[$metaGroup]) ? $this->metaGroups[$metaGroup] : []);
        }

        return $fields;
    }

    /**
     *
     * @param $metaGroup
     * @param bool $removeNulls
     * @return array
     */
    public function getValuesByMetaGroup(string $metaGroup, bool $removeNulls=false) : array
    {
        $fields                 = $this->getFieldsByMetaGroup($metaGroup);
        $values                 = array_intersect_key($this->data, array_flip($fields));
        if ( ! $removeNulls )
        {
            return $values;
        }
        return array_filter($values, function($val) {

            return ! is_null($val);
        });
    }

    /**
     * @param bool $includeTypes
     * @return array
     */
    public function getAllFields(bool $includeTypes=false) : array
    {
        return $includeTypes ? $this->schema : array_keys($this->schema);
    }

    /**
     * @param bool $includeTypes
     * @return array
     */
    public function getStructFields(bool $includeTypes=false) : array
    {
        return $includeTypes ? $this->structFields : array_keys($this->structFields);
    }

    /**
     * @return array
     */
    public function getAliasedFields() : array
    {
        $aliasedFields = [];
        foreach ( $this->getAllFields() as $field )
        {
            if ( preg_match('/_id$/', $field) )
            {
                $alias = preg_replace('/_id$/', '', $field);
                if ( $this->isField($alias) )
                {
                    $aliasedFields[$field] = $alias;
                }
            }
        }

        return $aliasedFields;
    }

    /**
     * @param string $field
     * @param bool|true $includeCollections
     * @return bool|string
     */
    public function getStructFieldType(string $field, bool $includeCollections=true) : string
    {
        if ( $includeCollections || ! isset($this->structFields[$field]) )
        {
            return isset($this->structFields[$field]) ? $this->structFields[$field] : '';
        }

        return ! preg_match('/Collection$/', $this->structFields[$field]) ? $this->structFields[$field] : '';
    }


    protected function _populateFieldProperties()
    {
        $this->data             = [];
        foreach ( $this->schema as $field => $meta )
        {
            $metaGroups             = explode('|', $meta);
            foreach ( $metaGroups as $group )
            {
                $this->metaGroups[$group]   = array_key_exists($group, $this->metaGroups) ? $this->metaGroups[$group] : [];
                $this->metaGroups[$group][] =  $field;
            }
            $structTypes            = preg_grep('/(Struct|StructCollection)$/', $metaGroups);
            foreach ( $structTypes as $structType )
            {
                $this->structFields[$field] = $structType;
            }
        }
    }


    public function isField(string $name, string $metaGroup=null) : bool
    {
        $fields                 = ! is_null($metaGroup) ? $this->getFieldsByMetaGroup($metaGroup) : $this->getFields();

        return in_array($name, $fields);
    }


    public function getFieldType(string $name) : string
    {
        $fields                 = $this->getFields(true);
        Assert::that($fields)->keyExists($name);
        $list                   = explode('|', $fields[$name]);

        return array_shift($list);
    }

    /**
     * @param string[] $names
     */
    public function assertFieldsExists(array $names)
    {
        foreach ( $names as $name )
        {
            Assert::that($this->isField($name))->true("The field '{$name}' does not exist");
        }
    }

    /**
     * @return array|Closure[]
     */
    protected function _getFieldHandlers() : array
    {
        return [];
    }

    /**
     * @param string $field
     * @return Closure
     */
    protected function _getFieldHandler($field) : Closure
    {
        $handlers = $this->_getFieldHandlers();
        if ( empty($handlers[$field]) || ! is_callable($handlers[$field]) )
        {
            return function($name, $value){
                unset($name);

                return $value;
            };
        }

        return $handlers[$field];
    }


    /**
     * @param string $name
     * @param mixed $value
     * @return StructInterface
     */
    public function __set(string $name, $value) : StructInterface
    {
        return $this->set($name, $value);
    }

    /**
     * @param $name
     */
    public function __unset(string $name)
    {
        unset($this->data[$name]);
    }

    /**
     * @param string|null $name
     * @return mixed|stdClass
     */
    public function get(string $name=null)
    {
        Assert::that($name)->nullOr()->string();
        if ( is_null($name) )
        {
            return (object)$this->data;
        }
        Assert::that($name)->inArray($this->getAllFields());
        if ( isset($this->data[$name]) )
        {
            return $this->data[$name];
        }

        return $this->_fixType($name, null);
    }

    /**
     * @param string|null $field
     * @param bool $returnDiff
     * @return array|mixed|object
     */
    public function getChanged(string $field=null, bool $returnDiff=false)
    {
        if ( ! is_null($field) )
        {
            Assert::that(isset($this->dirty[$field]))->true('The field is not marked as changed');
        }
        if ( $returnDiff )
        {
            return ! is_null($field) ? $this->dirty[$field] : $this->dirty;
        }
        if ( $field )
        {
            return (object)[$field, $this->get($field)];
        }
        $changed = [];
        foreach ( $this->dirty as $field => $value )
        {
            $changed[$field] = $this->get($field);
        }

        return (object)$changed;
    }

    /**
     * @param array $fields
     * @return stdClass
     */
    public function getChangedFields(array $fields) : stdClass
    {
        $changed = [];
        foreach ( $this->dirty as $field => $values )
        {
            if ( in_array($field, $fields) )
            {
                $changed[$field] = $this->get($field);
            }
        }

        return (object)$changed;
    }

    /**
     * @return array
     */
    public function getArray() : array
    {
        return $this->data;
    }

    /**
     * @return array
     */
    public function toArray() : array
    {
        $data = $this->getArray();
        foreach ( $data as $field => $value )
        {
            if ( is_object($value) )
            {
                $data[$field] = method_exists($value, 'toArray') ? $value->toArray() : (array)$value;
            }
        }

        return $data;
    }


    /**
     * @param string $name
     * @return mixed
     */
    public function __get(string $name)
    {
        return $this->get($name);
    }

    /**
     * @param $name
     * @return bool
     */
    public function __isset(string $name) : bool
    {
        return array_key_exists($name, $this->data);
    }

    /**
     * @param $name
     * @return bool
     */
    public function isPopulated(string $name) : bool
    {
        return array_key_exists($name, $this->data);
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return $this->strip();
    }

    /**
     * @return array
     */
    public function strip() : array
    {
        $data = $this->data;
        foreach ( $this->stripOnSerialize as $field )
        {
            unset($data[$field]);
        }

        return $data;
    }

    /**
     * @param array $keys
     * @return array
     */
    public function getLoggingValues(array $keys=[]) : array
    {
        Assert::that($keys)->isArray();
        $logables   = $this->getFieldsByMetaGroup(self::LOGABLE);
        $logables   = array_merge($logables, $keys);
        $logables   = array_unique($logables);
        $data       = [];
        foreach ( $logables as $key )
        {
            $data[$key] = $this->get($key);
        }

        return $data;
    }


    protected static function _date(string $timestamp=null, string $format='Y-m-d') : string
    {
        $timestamp = is_string($timestamp) ? strtotime($timestamp) : $timestamp;

        return is_null($timestamp) ? date($format) : date($format, $timestamp);
    }


    protected static function _dateTime(string $timestamp=null, string $format='Y-m-d H:i:s') : string
    {
        $timestamp = is_string($timestamp) ? strtotime($timestamp) : $timestamp;

        return is_null($timestamp) ? date($format) : date($format, $timestamp);
    }
}
