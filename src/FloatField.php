<?php declare(strict_types=1);


namespace Terah\Types;


use Closure;

class FloatField extends Field
{
    public function __construct(string $name, Closure $validator, $default=null)
    {
        parent::__construct($name, 'float', $validator, $default);
    }
}