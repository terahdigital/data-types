<?php declare(strict_types=1);

namespace Terah\Types;


interface ValueCollectionInterface
{
    public function getEntityType() : string;

    /**
     * @param object $data
     */
    public function append($data);

//    /**
//     * @param array   $data
//     * @param string|array $types
//     * @param bool|false $permissive
//     * @return array
//     */
//    public function filterData(array $data, $types=null, bool $permissive=false) : array;
//
//    /**
//     * @param bool $hard
//     * @return StructCollectionInterface
//     */
//    public function reset(bool $hard=false) : StructCollectionInterface;
//
//    /**
//     * @return bool
//     */
//    public function isUnchanged() : bool;

    /**
     * @return array
     */
    public function jsonSerialize();
}