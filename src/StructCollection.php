<?php declare(strict_types=1);

namespace Terah\Types;

use ArrayObject;
use IteratorAggregate;
use JsonSerializable;
use Terah\Assert\Assert;

/**
 * Class StructCollection
 *
 * @package Terah\Types
 */
class StructCollection extends ArrayObject implements IteratorAggregate, JsonSerializable, StructCollectionInterface
{
    /** @var string */
    protected $entityType   = '';

    /**
     * StructCollection constructor.
     *
     * @param array $values
     * @param bool  $permissive
     */
    public function __construct(array $values=[], bool $permissive=false)
    {
        $entityType             = $this->getEntityType();
        $values                 = ! is_null($values) ? $values : [];
        foreach ( $values as $value )
        {
            $value                  = $value instanceof $entityType ? $value : new $entityType($value, $permissive);
            $this->append($value);
        }
    }

    /**
     * @return string
     */
    public function getEntityType() : string
    {
        if ( ! $this->entityType )
        {
            $this->entityType       = preg_replace('/Collection$/', '', get_called_class());
        }

        return $this->entityType;
    }

    /**
     * @param object $value
     */
    public function append($value)
    {
        Assert::that($value)->isInstanceOf($this->entityType);

        parent::append($value);
    }

    /**
     * @param array   $data
     * @param string|array $types
     * @param bool|false $permissive
     * @return array
     */
    public function filterData(array $data, $types=null, bool $permissive=false) : array
    {
        foreach ( $data as $idx => $object )
        {
            /** @var Struct $struct */
            $entityType             = $this->getEntityType();
            $struct                 = new $entityType();
            $object                 = is_array($object) ? (object)$object : $object;
            $data[$idx]             = $struct->filterData($object, $types, $permissive);
        }

        return $data;
    }

    /**
     * @param bool $hard
     * @return StructCollectionInterface
     */
    public function reset(bool $hard=false) : StructCollectionInterface
    {
        foreach ( $this->getArrayCopy() as $struct )
        {
            /** @var Struct $struct */
            $struct->reset($hard);
        }

        return $this;
    }

    /**
     * @return bool
     */
    public function isUnchanged() : bool
    {
        foreach ( $this->getArrayCopy() as $struct )
        {
            /** @var Struct $struct */
            if ( ! $struct->isUnchanged() )
            {
                return false;
            }
        }

        return true;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        //return $this->data;
        return $this->getArrayCopy();
    }
}