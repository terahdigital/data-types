<?php declare(strict_types=1);

namespace Terah\Types;


use Terah\Assert\Assert;

class IdObjArr extends Arr
{
    /**
     * @param int $key
     * @param object $value
     */
    public function offsetSet($key, $value)
    {
        Assert::that($key)->int();
        Assert::that($value)->isObject();

        parent::offsetSet($key, $value);
    }

    /**
     * @param int $key
     * @return object
     */
    public function offsetGet($key)
    {
        Assert::that($key)->int();

        return parent::offsetGet($key);
    }

}