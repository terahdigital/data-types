<?php declare(strict_types=1);

namespace Terah\Types\Tests;

use Terah\Types\DTO;

/**
 * Class ExampleDTO
 *
 * @package Terah\Types\Tests
 * @property int id
 * @property string name
 * @property string email
 */
class ExampleDTO extends DTO
{
    protected function populateSchema() : DTO
    {
        $this->schema = [
            'id'            => function ($field, $value) {

                $this->assert($value)
                    ->fieldName($field)
                    ->emptyOr()
                    ->id('ID must be a valid integer id, (%s) submitted.');
            },
            'name'     => function ($field, $value) {

                $this->assert($value)
                    ->fieldName($field)
                    ->emptyOr()
                    ->string('Source ID must be a valid string, (%s) submitted.');
            },
            'email' => function($field, $value) {

                $this->assert($value)
                    ->fieldName($field)
                    ->emptyOr()
                    ->utf8('The value has to be UTF-8 characters only, (%s) submitted.')
                    ->maxLength(255, 'Email cannot exceed 255 characters, (%s) submitted.')
                    ->email('Email must be a valid email address.');
            }
        ];

        return $this;
    }
}