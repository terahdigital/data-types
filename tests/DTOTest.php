<?php declare(strict_types=1);

namespace Terah\Types\Tests;

use Exception;
use PHPUnit_Framework_TestCase;

require_once __DIR__ . '/../../../autoload.php';
require_once 'ExampleDTO.php';

class DTOTest extends PHPUnit_Framework_TestCase
{
    protected $validData = [
        'id'    => 234,
        'name'  => 'Fred Flintstone',
        'email' => 'fred@example.com',
    ];

    public function testConstructor()
    {
        $value = new ExampleDTO();
        static::assertEquals([], $value->getArrayCopy());
    }

    public function testTypeIsStdClass()
    {
        $value = new ExampleDTO();
        $value = $this->typeTest($value);

    }

    protected function typeTest($obj)
    {
        return $obj;
    }

    public function testEmptyFactory()
    {
        $value = ExampleDTO::factory();
        static::assertEquals([], $value->getArrayCopy());
    }

    public function testNotEmptyFactory()
    {
        $value = ExampleDTO::factory($this->validData);
        static::assertEquals($this->validData, $value->getArrayCopy());
    }

    public function testPropertySetting()
    {
        $value              = new ExampleDTO();
        $value->id          = $this->validData['id'];
        $value->name        = $this->validData['name'];
        $value->email       = $this->validData['email'];
        static::assertEquals($this->validData, $value->getArrayCopy());
    }

    public function testArraySetting()
    {
        $value              = new ExampleDTO();
        $value['id']        = $this->validData['id'];
        $value['name']      = $this->validData['name'];
        $value['email']     = $this->validData['email'];
        static::assertEquals($this->validData, $value->getArrayCopy());
    }

    public function testPropertyGetting()
    {
        $value = new ExampleDTO($this->validData);
        static::assertEquals($this->validData['id'], $value->id);
        static::assertEquals($this->validData['name'], $value->name);
        static::assertEquals($this->validData['email'], $value->email);
        static::assertEquals($this->validData, $value->getArrayCopy());
    }

    public function testArrayGetting()
    {
        $value = new ExampleDTO($this->validData);
        static::assertEquals($this->validData['id'], $value['id']);
        static::assertEquals($this->validData['name'], $value['name']);
        static::assertEquals($this->validData['email'], $value['email']);
        static::assertEquals($this->validData, $value->getArrayCopy());
    }

    public function testJsonSerialise()
    {
        $value = new ExampleDTO($this->validData);
        static::assertEquals($this->validData['id'], $value['id']);
        static::assertEquals($this->validData['name'], $value['name']);
        static::assertEquals($this->validData['email'], $value['email']);
        static::assertEquals(json_encode($this->validData, JSON_PRETTY_PRINT), json_encode($value->getArrayCopy(), JSON_PRETTY_PRINT));
    }

    public function testPropertyValidationThrowsException()
    {
        $callback = function() {

            $value = new ExampleDTO($this->validData);
            $value->email = 'asdfasdf';
        };
        static::assertException($callback, 'Terah\Assert\AssertionFailedException');
    }

    public function testArrayValidationThrowsException()
    {
        $callback = function() {

            $value = new ExampleDTO($this->validData);
            $value['email'] = 'asdfasdf';
        };
        static::assertException($callback, 'Terah\Assert\AssertionFailedException');
    }

    public function testMissingPropertyThrowsException()
    {
        $callback = function() {

            $value = new ExampleDTO($this->validData);
            $value->notExists = 'asdfasdf';
        };
        static::assertException($callback, 'Terah\Assert\AssertionFailedException');
    }

    public function testMissingArrayThrowsException()
    {
        $callback = function() {

            $value = new ExampleDTO($this->validData);
            $value['notExists'] = 'asdfasdf';
        };
        static::assertException($callback, 'Terah\Assert\AssertionFailedException');
    }

    protected static function assertException(callable $callback, $expectedException = 'Exception', $expectedCode = null, $expectedMessage = null)
    {
        if ( ! class_exists($expectedException) && !interface_exists($expectedException) )
        {
            static::fail("An exception of type '$expectedException' does not exist.");
        }

        try {
            $callback();
        }
        catch (Exception $e)
        {
            $class      = get_class($e);
            $message    = $e->getMessage();
            $code       = $e->getCode();

            $extraInfo  = $message ? " (message was $message, code was $code)" : ($code ? " (code was $code)" : '');
            static::assertInstanceOf($expectedException, $e, "Failed asserting the class of exception$extraInfo.");

            if ( $expectedCode !== null )
            {
                static::assertEquals($expectedCode, $code, "Failed asserting code of thrown $class.");
            }
            if ( $expectedMessage !== null )
            {
                static::assertContains($expectedMessage, $message, "Failed asserting the message of thrown $class.");
            }
            return;
        }

        $extraInfo = $expectedException !== 'Exception' ? " of type $expectedException" : '';
        static::fail("Failed asserting that exception$extraInfo was thrown.");
    }
}
